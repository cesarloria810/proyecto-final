console.log('Cargando Cards...');
const dataCards = [

    {
        "title": "Traditional Tattoo",
        "url_image": "https://i.pinimg.com/236x/08/9f/c0/089fc08c64221a89a438f16541029404.jpg",
        "desc": "Aquí encontrarás toda la información sobre el mundo del tatuaje tradicional: historia, estilo e inspiración para que conozcas a fondo este apasionante mundo.",       
        "cta": "Mostrar mas...",
        "link": "https://labellasolera.com/blog/tatuaje-tradicional/"
    },
    {
        "title": "Neotraditional Tattoo",
        "url_image": "https://i.pinimg.com/236x/24/cd/04/24cd04f11e13fab89134b1e38421d5b2.jpg",
        "desc": "Esta mezcla de dos estilos tan empleados como el old y el new school hace que en ocasiones sea complicado reconocer un tatuaje neotradi. Por suerte podemos esbozar sus rasgos más característicos",
        "cta": "Mostrar mas...",
        "link": "https://inksweettattoo.es/caracteristicas-del-tatuaje-neotradicional/"
    },
    {
        "title": "Realistic Tattoo",
        "url_image": "https://i.pinimg.com/236x/d1/bb/7c/d1bb7ccd1d2fcc3e112fcd9f4908e490.jpg",
        "desc": "El estilo de tatuaje realista es uno de los más clásicos y con más recorrido dentro del mundo del tatuaje.",       
        "cta": "Mostrar mas...",
        "link": "https://logiabarcelona.com/tatuaje/realismo-tattoo/"
    },
    {
        "title": "Trash Polka Tattoo",
        "url_image": "https://i.pinimg.com/236x/c4/9e/8a/c49e8a3d47653895b8b5adf9ba9054c2.jpg",
        "desc": "Tonos negros y rojos y diseños a caballo entre lo real y lo abstracto, esas son las características que mejor definen el estilo trash polka",       
        "cta": "Mostrar mas...",
        "link": "https://inksweettattoo.es/el-estilo-trash-polka-tatuajes-entre-lo-real-y-lo-abstracto/"
    },
    {
        "title": "Black and Grey Tattoo",
        "url_image": "https://i.pinimg.com/236x/d9/2b/5b/d92b5b2babef7a78b877c2298bca7831.jpg",
        "desc": " El artista trabaja solo con tinta negra, y la diluye con más o menos agua para obtener distintas tonalidades de gris y crear profundidad, sombreados…",       
        "cta": "Mostrar mas...",
        "link": "https://es.tattoofilter.com/tatuajes/tatuajes-en-black-and-grey"
    },
    {
        "title": "Dotwork Tattoo (Puntillismo)",
        "url_image": "https://i.pinimg.com/236x/88/b6/14/88b61470dfa955f03f9a3d6010aa7a49.jpg",
        "desc": "El puntillismo es un estilo pictórico o técnica artística que consiste en realizar los trazos del dibujo con diminutos puntos, dejando de lado las líneas o las grandes zonas sombreadas",       
        "cta": "Mostrar mas...",
        "link": "https://logiabarcelona.com/tatuajes/puntillismo/"
    },
    {
        "title": "Anime Tattoo",
        "url_image": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ3RR6FJ3HMGQ-SpuwvsQ57m4ps-Zyq7fJIEg&usqp=CAU",
        "desc": "Con un legado de cientos de años que hace referencia a las historietas o comics en general; la palabra “anime” se ha derivado de “animación” realizado por medio de computadoras.",       
        "cta": "Mostrar mas...",
        "link": "https://logiabarcelona.com/tatuaje/tatuajes-de-animes/"
    },
    {
        "title": "Japonés Tradicional y Neo Japonés",
        "url_image": "https://i.pinimg.com/originals/d4/10/1b/d4101b7f6f838b40771d2b0c66fe9b5f.jpg",
        "desc": "Los tatuajes japoneses tienen algo especial, más allá de conservar una fuerte conexión entre el arte popular de la cultura oriental y la estrecha relación con la historia del tatuaje en general.",       
        "cta": "Mostrar mas...",
        "link": "https://piercingytatuajes.com/tatuajes-japoneses"
    }];

(function() {
    let CARD = {
        init: function (){
            console.log('El modulo de carga funciona correctamente');
            let _self = this;

            //llamamos a las funciones 
            this.insertData(_self);
        },
        eventHandler: function (_self){
            let arrayRefs = document.querySelectorAll('.accordion-title');

            for (let x = 0; x <arrayRefs.length; x++){
                arrayRefs[x].addEventListener('click', function(event){
                    console.log('Evento: ', event);
                    _self.showTab(event.target);
                });
            }

        },
        insertData: function (_self){
            dataCards.map(function(item, index){
                document.querySelector('.card-list').insertAdjacentHTML('beforeend', _self.tplCardItem(item, index)
                );
            });
        },

        tplCardItem: function (item, index){
            return(`<div class='card-item' id"card-number-${index}">
            <img src="${item.url_image}"/>

            <div class="card-info">
              <p class='card-title'>${item.title} </p>
              <p class='card-desc'>${item.desc} </p>
              <a class ='card-cta' target='blank' href="${item.link}">${item.cta}<a/>
            </div>
            </div> `)
        },
    }

    CARD.init();
})();