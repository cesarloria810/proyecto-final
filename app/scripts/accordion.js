console.log('Cargando Accordion...');
const dataAccordion = [
    {   "title": "¿Qué es un tatuaje?",
    "desc": "La piel humana se compone de tres capas, una de ellas es la epidermis que está localizada en la superficie, una capa intermedia denominada la dermis y una profunda conocida como la hipodermis (Whittle & Baldassare, 2004). Debido al ataque constante que recibe por parte de bacterias, la epidermis cambia constantemente sus células; la hipodermis se encarga de la producción de melanina, una sustancia química que pigmenta el tejido cutáneo y la dermis nutre y brinda apoyo mecánico a la epidermis evitando daños e infecciones (Palomino, 2001; Rodríguez, 1998). De acuerdo con lo anterior, hacer un tatuaje requiere de la inyección de una cantidad abundante de pigmentos (tintes) especializados en la capa intermedia la cual no renueva sus células garantizando que la tinta permanezca allí; para esto se utiliza una maquina creada por el artista e inventor estadounidense Samuel O’Reilly a fines del 1800 y que a través de una aguja esterilizada que penetra en la piel a una velocidad de 50 a 3000 veces por minuto, plasmando un diseño en la piel (Álvarez, 2000). Un gran porcentaje de individuos manifiesta dolor extremo al realizarse un tatuaje; esto se debe a que la dermis se encuentra conformada por una serie de fibras elásticas, nervios y receptores (nociceptores) que son los responsables de enviar señales al sistema nervioso central para su posterior interpretación (Cerveró, 2000). Si bien la práctica del tatuaje parece especializada es importante señalar el origen y posterior evolución de la misma. "},
    {   "title": "Historia del tatuaje",
    "desc": "Se cree que los tatuajes más antiguos fueron hechos hace 5300 años; prueba de esto son los restos momificados de un cazador del neolítico conocido con el nombre de “Oetzi” el cual presentaba tatuajes en espalda y rodillas. El hallazgo se realizó en un glaciar de los Alpes situado en la frontera entre Austria e Italia (Brena, 2007). Otra línea de evidencia procede de algunos huesos con aspecto puntiagudo encontrados en la cueva de Aurignac que al parecer fueron usados para hacer marcas cutáneas (Ganter, 2005). El tatuaje hizo parte de la cultura egipcia donde los grandes faraones se pigmentaban la piel por razones artísticas; aun su significado es un misterio. Algunos diseños encontrados en los sarcófagos tienen vigencia en la actualidad (Tesone, 2000).Hacia el año 1000 A.C. se establecío la práctica de tatuarse en la cultura oriental. Ganter (2005) postula que las rutas comerciales entre la India, la China y el Japón son responsables de la introducción del tatuaje en estas sociedades, donde más adelante adquiriría un significado ritual; incluso, en algunas regiones carentes de recursos económicos el tatuaje se convirtió en una forma de imitar el kimono (Ganter, 2005). Con el tiempo, esta tendencia se convertiría en una tradición familiar caracterizada por marcar la piel de los hijos a partir de los tres hasta los 25 años de edad cuando el cuerpo quedaba completamente tatuado, generando así una percepción y definición diferente a la de un sujeto sin modificaciones corporales(Cantero, 2008).Según Cassab (2002), el tatuaje en China comenzó a ser fuente de información no verbal permitiendo identificar a individuos particulares para que de esta forma se les ofreciera un determinado trato, dependiendo del color, la cantidad y la región donde fuese ubicado podía representar rasgos de belleza, estado civil y actividades o trabajos específicos. De forma paralela los japoneses usaron la práctica del tatuaje como herramienta de discriminación hacía los esclavos y los presos, de esta manera todo individuo que cometiera un acto indebido podía ser identificado y juzgado puesto que no existía forma de borrar dicha marca (Cassab, 2002)."},
    {   "title": "Origen",
    "desc": "Ganter (2005) menciona que al transformarse el tatuaje, los diseños plasmados en la piel tomaron un rumbo hacia lo extravagante y lo excéntrico generando una impresión antiestética y agresiva de sus portadores, sin contar el número de casos en los que se confundía un tatuaje por castigo delincuencial y el tatuaje realizado por voluntad propia. Por esta razón, el emperador Matsuhito (1876-1912) prohibió la práctica de tatuarse en vista de la apertura de Japón al occidente, esto con el fin de no generar impresión de salvajismo ante los extranjeros y anexo a ello posibilitar la expansión del comercio y cultura de la región (Pérez & Castillo, 2013).Para los Griegos y Romanos el tatuaje era útil para señalar el rango y la posición social, además servía para diferenciar jerarquías militares y la propiedad de un esclavo (Hermosillo, Tovar, Gomez-Valdés, Herrera, & Sánchez-Mejoradaa, 2011)Rodríguez-García, Aguilar-Ye, Rodríguez-Silva, y Rodríguez-Guzmán, (2012) afirman que gracias al capitán James Cook, navegante y explorador británico, se comenzó a dispersar la práctica de tatuarse en la cultura occidental. Cook era famoso por sus viajes a través del océano pacifico y tenía como costumbre plasmar en su cuerpo símbolos y coordenadas que advirtieran sobre las experiencias vividas en cada lugar que visitó (Ganter, 2005). La idea se origina en uno de sus viajes al tener contacto con la cultura polinesia, observó que esta tenía por tradición golpear con un hueso acanalado la piel TATUAJE E IMAGEN CORPORAL 105 de algunos de sus integrantes para generar una marca imborrable (Cassab, 2002)."},
];

(function () {
    let ACCORDION = {
        init: function(){
            let _self = this;
            //Llamamos la funcion
            this.insertData(_self);
            this.eventHandler(_self);
        },

        eventHandler: function (_self){
            let arrayRefs = document.querySelectorAll('.accordion-title');

            for (let x = 0; x < arrayRefs.length; x++){
            arrayRefs[x].addEventListener('click', function(event){
                console.log('event', event);
                _self.showTab(event.target);
              });
            } 
        },

        showTab: function(refItem){
            let activeTab = document.querySelector('.tab-active');

            if(activeTab){
                activeTab.classList.remove('tab-active');
            }

            console.log('show tab', refItem);
            refItem.parentElement.classList.toggle('tab-active');
        },


        insertData: function (_self){
            dataAccordion.map(function (item, index){
                document.querySelector('.main-accordion-container').insertAdjacentHTML('beforeend', _self.tplAccordionItem(item));
            });
        },

        tplAccordionItem: function (item) {
            return(`<div class='accordion-item'>
            <div class='accordion-title'><p>${item.title}</p></div>
            <div class='accordion-desc'><p>${item.desc}<p></div>
            </div>`)
        },
        

    }
    ACCORDION.init();
})();